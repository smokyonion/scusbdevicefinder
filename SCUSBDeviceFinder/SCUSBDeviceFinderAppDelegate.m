//
//  SCUSBDeviceFinderAppDelegate.m
//  SCUSBDeviceFinder
//
//  Created by Vincent S. Wang on 6/22/11.
//  Copyright 2011 Vincent S. Wang. All rights reserved.
//

#import "SCUSBDeviceFinderAppDelegate.h"


@implementation SCUSBDeviceFinderAppDelegate

@synthesize window;
@synthesize deviceList;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [deviceList removeAllItems];
    
    [[SCUSBDeviceFinder sharedUSBDeviceFinder] setDelegate:self];
    [[SCUSBDeviceFinder sharedUSBDeviceFinder] addSearchForVendorID:VENDOR_ID productID:PRODUCT_ID];
    //[[SCUSBDeviceFinder sharedUSBDeviceFinder] addAllUSBDevices];
}

- (void)didAddUSBDevice:(NSDictionary *)info
{
    [info retain];
#ifdef DEBUG
    NSLog(@"Device Added: %@(pid:%@ vid:%@)", 
          [info objectForKey:kSCDevicePropertyProductName], [info objectForKey:kSCDevicePropertyProductID], [info objectForKey:kSCDevicePropertyVendorID]);
#endif
    
    NSString *name = [info objectForKey:kSCDevicePropertyProductName];
    if ([name length] == 0) name = @"Unknown USB Device";
    [deviceList addItemWithTitle:name];
    [deviceList selectItemWithTitle:name]; // select the new device
    
    NSLog(@"%@", info);
    
    [info release];
}

- (void)didRemoveUSBDevice:(NSDictionary *)info
{
    [info retain];
    NSLog(@"Device Removed: %@(pid:%@ vid:%@)", 
          [info objectForKey:kSCDevicePropertyProductName], [info objectForKey:kSCDevicePropertyProductID], [info objectForKey:kSCDevicePropertyVendorID]);
    
    // Actually, this is not a good practice. Because it is possible that two devices with the same product name been plugged in/out at the same time
    NSString *name = [info objectForKey:kSCDevicePropertyProductName];
    if ([name length] == 0) name = @"Unknown USB Device";
    [deviceList removeItemWithTitle:name];

    [info release];
}

@end
