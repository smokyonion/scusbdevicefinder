//
//  SCUSBDeviceFinderAppDelegate.h
//  SCUSBDeviceFinder
//
//  Created by Vincent S. Wang on 6/22/11.
//  Copyright 2011 Vincent S. Wang. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SCUSBDeviceFinder.h"

@interface SCUSBDeviceFinderAppDelegate : NSObject <NSApplicationDelegate, SCUSBDeviceFinderDelegate> {
@private
    NSWindow *window;
    NSPopUpButton *deviceList;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSPopUpButton *deviceList;

@end
