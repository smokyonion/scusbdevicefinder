//
//  SCUSBDeviceFinder.m
//  SCUSBDeviceFinder
//
//  Created by Vincent S. Wang on 6/22/11.
//  Copyright 2011 Vincent S. Wang. All rights reserved.
//

#import <IOKit/usb/IOUSBLib.h>
#import <IOKit/serial/IOSerialKeys.h>
#import <IOKit/IOBSD.h>
#import <IOKit/IOMessage.h>
#import <IOKit/IOCFPlugIn.h>
#import "SCUSBDeviceFinder.h"

// IOKit API online ref: http://developer.apple.com/mac/library/documentation/DeviceDrivers/Conceptual/AccessingHardware/AH_IOKitLib_API/AH_IOKitLib_API.html

// Handy key for delegate to query properties in dictionary
NSString *const kSCDevicePropertyVendorName = @"USB Vendor Name";
NSString *const kSCDevicePropertyProductName = @"USB Product Name";
NSString *const kSCDevicePropertyVendorID = @"idVendor";
NSString *const kSCDevicePropertyProductID = @"idProduct";

@interface SCUSBDeviceFinder ()
- (void)postDeviceAddedNotification:(io_service_t)aDevice;
- (void)postDeviceRemovedNotification:(io_service_t)aDevice;
@end


// these are callback function whens a USB device we care about is connected/disconnected 
// they call the delegate
static void SCUSBFinderDeviceAdded(void *refCon, io_iterator_t iterator)
{
    SCUSBDeviceFinder *deviceFinder = (SCUSBDeviceFinder *)refCon;
    io_service_t usbDevice;
    while ( (usbDevice = IOIteratorNext(iterator)) ) {		
        [deviceFinder postDeviceAddedNotification:usbDevice];
        IOObjectRelease(usbDevice);
    }
}

static void SCUSBFinderDeviceRemoved(void *refCon, io_iterator_t iterator)
{
    SCUSBDeviceFinder *deviceFinder = (SCUSBDeviceFinder *)refCon;
    io_service_t usbDevice;
    while ( (usbDevice = IOIteratorNext(iterator)) ) {		
        [deviceFinder postDeviceRemovedNotification:usbDevice];
        IOObjectRelease(usbDevice);
    }
}

@interface SCUSBDeviceFinder ()

@property(nonatomic, readonly) IONotificationPortRef notificationPort;

@end

@implementation SCUSBDeviceFinder

@synthesize delegate = _delegate;
@synthesize notificationPort = _notificationPort;

+ (SCUSBDeviceFinder *)sharedUSBDeviceFinder
{
    static SCUSBDeviceFinder *singletonFinder = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonFinder = [[SCUSBDeviceFinder alloc] init];
    });
    
    return singletonFinder;
}

- (id)init
{
    self = [super init];
    if (!self) return nil;
    
    CFRunLoopSourceRef runLoopSource;
    
    // Create a notification port and add its run loop event source to our run loop
    // (can be used for all devices later), using default master port
    // This is how async notifications get set up.
    _notificationPort = IONotificationPortCreate(kIOMasterPortDefault);
    
    // get runloop source for this port
    runLoopSource = IONotificationPortGetRunLoopSource(_notificationPort);
    // add source to current runloop
    CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, kCFRunLoopDefaultMode);
    
    return self;
}



- (void)dealloc
{
    // destroy notification port and deregistering all device notifications
    IONotificationPortDestroy(_notificationPort);
    _notificationPort = NULL;
    [super dealloc];
}

- (BOOL)addAllUSBDevices
{
    CFMutableDictionaryRef 	matchingDict;
	
    // Create a matching dictionary
	matchingDict = IOServiceMatching(kIOUSBDeviceClassName); // Interested in instances of class
    // IOUSBDevice and its subclasses
    if (!matchingDict) {
        NSLog(@"Can't create a USB matching dictionary.");
		return NO;
    }

    
	io_iterator_t addedIter;
    
    
    // Now set up a notification to be called when a device is first matched by I/O Kit.
    // Note that this will not catch any devices that were already plugged in so we take
    // care of those later.
	// notifyPort, notificationType, matching, callback, refCon, notification
    IOServiceAddMatchingNotification(self.notificationPort, kIOFirstMatchNotification,
									 (CFDictionaryRef)CFRetain(matchingDict), SCUSBFinderDeviceAdded, self, &addedIter);		
    
    // Iterate once to get already-present devices and arm the notification
    SCUSBFinderDeviceAdded(self, addedIter);
    
    
    // register for device detachment - ignore return value since we cannot do much about a failure anyway
    IOServiceAddMatchingNotification(self.notificationPort, kIOTerminatedNotification, 
                                     matchingDict, SCUSBFinderDeviceRemoved, self, &addedIter);
    
    // arm notification
    SCUSBFinderDeviceRemoved(self, addedIter);
    
	return YES;
}

- (BOOL)addSearchForVendorID:(int)usbVendor productID:(int)usbProduct
{
    CFMutableDictionaryRef 	matchingDict;
    CFNumberRef				numberRef;
	
    // Create a matching dictionary
	matchingDict = IOServiceMatching(kIOUSBDeviceClassName); // Interested in instances of class
    // IOUSBDevice and its subclasses
    if (!matchingDict) {
        NSLog(@"Can't create a USB matching dictionary.");
		return NO;
    }
	
	numberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &usbVendor);
    CFDictionarySetValue(matchingDict, CFSTR(kUSBVendorID), numberRef);
    CFRelease(numberRef);
 	
    numberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &usbProduct);
    CFDictionarySetValue(matchingDict, CFSTR(kUSBProductID), numberRef);
    CFRelease(numberRef);
	
    numberRef = 0;
    
	io_iterator_t addedIter;
    
    
    // Now set up a notification to be called when a device is first matched by I/O Kit.
    // Note that this will not catch any devices that were already plugged in so we take
    // care of those later.
	// notifyPort, notificationType, matching, callback, refCon, notification
    IOServiceAddMatchingNotification(self.notificationPort, kIOFirstMatchNotification,
									 (CFDictionaryRef)CFRetain(matchingDict), SCUSBFinderDeviceAdded, self, &addedIter);		
    
    // Iterate once to get already-present devices and arm the notification
    SCUSBFinderDeviceAdded(self, addedIter);
    

    // register for device detachment - ignore return value since we cannot do much about a failure anyway
    IOServiceAddMatchingNotification(self.notificationPort, kIOTerminatedNotification, 
                                     matchingDict, SCUSBFinderDeviceRemoved, self, &addedIter);
    
    // arm notification
    SCUSBFinderDeviceRemoved(self, addedIter);
    
	return YES;
}

- (void)addDeviceFromDictionary:(NSDictionary *)matchingDictionary
{
    io_iterator_t addedIter;

    // register for device attachment - retain the matchingDictionary, since it is consumed by the call!
    IOServiceAddMatchingNotification(self.notificationPort, kIOFirstMatchNotification, (CFDictionaryRef)[matchingDictionary retain],
                                     SCUSBFinderDeviceAdded, self, &addedIter);
    // arm notification
    SCUSBFinderDeviceAdded(self, addedIter);
    // register for device detachment - ignore return value since we cannot do much about a failure anyway
    IOServiceAddMatchingNotification(self.notificationPort, kIOTerminatedNotification, (CFDictionaryRef)[matchingDictionary retain],
                                     SCUSBFinderDeviceRemoved, self, &addedIter);
    // arm notification
    SCUSBFinderDeviceRemoved(self, addedIter);
}

#pragma mark -
#pragma mark Device Added/Removed Notification - Called by the callback functions

- (void)postDeviceAddedNotification:(io_service_t)aDevice
{
#ifdef DEBUG
    NSLog(@"SCUSBDeviceFinder: device added notification received");
#endif
    if ([self.delegate respondsToSelector:@selector(didAddUSBDevice:)]) {
        
        // get device properties
        CFMutableDictionaryRef deviceProperties = NULL;
        if (IORegistryEntryCreateCFProperties(aDevice, &deviceProperties, kCFAllocatorDefault, kNilOptions) == KERN_SUCCESS) {
            [self.delegate performSelector:@selector(didAddUSBDevice:) withObject:(NSDictionary *)deviceProperties];
            CFRelease(deviceProperties);
        }
    }
    
}

- (void)postDeviceRemovedNotification:(io_service_t)aDevice
{
#ifdef DEBUG
    NSLog(@"SCUSBDeviceFinder: device removed notification received");
#endif
    if ([self.delegate respondsToSelector:@selector(didRemoveUSBDevice:)]) {
                
        // get device properties
        CFMutableDictionaryRef deviceProperties = NULL;
        if (IORegistryEntryCreateCFProperties(aDevice, &deviceProperties, kCFAllocatorDefault, kNilOptions) == KERN_SUCCESS) {
            [self.delegate performSelector:@selector(didRemoveUSBDevice:) withObject:(NSDictionary *)deviceProperties];
            CFRelease(deviceProperties);
        }
    }
}

@end

