//
//  main.m
//  SCUSBDeviceFinder
//
//  Created by Vincent S. Wang on 6/22/11.
//  Copyright 2011 Vincent S. Wang. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
