//
//  SCUSBDeviceFinder.h
//  SCUSBDeviceFinder
//
//  Created by Vincent S. Wang on 6/22/11.
//  Copyright 2011 Vincent S. Wang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/IOKitLib.h>
#import "SCUSBDeviceFinder.h"

#define VENDOR_ID 1133 // Logitech
#define PRODUCT_ID 49250 // USB Laser Mouse

// Handy keys for delegate to query properties in dictionary
extern NSString *const kSCDevicePropertyVendorName;
extern NSString *const kSCDevicePropertyProductName;
extern NSString *const kSCDevicePropertyVendorID;
extern NSString *const kSCDevicePropertyProductID;

@protocol SCUSBDeviceFinderDelegate <NSObject>
@required
- (void)didAddUSBDevice:(NSDictionary *)info;
- (void)didRemoveUSBDevice:(NSDictionary *)info;

@end

@interface SCUSBDeviceFinder : NSObject {
 @private
    id <SCUSBDeviceFinderDelegate> _delegate;
    IONotificationPortRef _notificationPort;
}

@property(assign) id <SCUSBDeviceFinderDelegate> delegate;

+ (SCUSBDeviceFinder *)sharedUSBDeviceFinder;
- (BOOL)addAllUSBDevices;
- (BOOL)addSearchForVendorID:(int)usbVendor productID:(int)usbProduct;
- (void)addDeviceFromDictionary:(NSDictionary *)matchingDictionary;

@end
